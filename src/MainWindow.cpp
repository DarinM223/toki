#include "MainWindow.h"
#include "moc_MainWindow.cpp"

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent) {
  ui_.setupUi(this);
  ui_.pushButton->setAutoDefault(false);

  connect(ui_.pushButton, SIGNAL(clicked()), this, SLOT(buttonClicked()));
}

void MainWindow::buttonClicked() {
  QMessageBox msg;
  msg.setText("Hello");

  msg.exec();
}
